package main

import (
	"deallsTest/controllers"
	"deallsTest/middleware"
	"deallsTest/models"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	r := gin.Default()

	db, err := gorm.Open("mysql", "root:@tcp(127.0.0.1:3306)/dealls_test?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	db.AutoMigrate(&models.User{}, &models.Profile{})

	r.POST("/signup", controllers.SignUp(db))
	r.POST("/login", controllers.Login(db))

	// Protected routes
	auth := r.Group("/")
	auth.Use(middleware.AuthMiddleware())
	{
		auth.POST("/create-profile", controllers.CreateProfile(db))
		auth.GET("/profiles", controllers.ViewProfiles(db))
		auth.POST("/swipe", controllers.Swipe(db))
		auth.POST("/purchase", controllers.Purchase(db))
		auth.POST("/logout", controllers.Logout())
	}

	r.Run(":8080")
}
