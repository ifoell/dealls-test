package models

import (
	"github.com/jinzhu/gorm"
)

type Profile struct {
	gorm.Model
	UserID uint
	Name   string
	Age    int
	Bio    string
}
