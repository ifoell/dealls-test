package models

import (
	"github.com/jinzhu/gorm"
)

type Swipe struct {
	gorm.Model
	UserID    uint
	ProfileID uint
	Type      string // "left" or "right"
	Date      string
}
