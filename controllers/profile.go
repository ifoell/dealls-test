package controllers

import (
	"deallsTest/models"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"net/http"
	"time"
)

func ViewProfiles(db *gorm.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		userID := c.MustGet("userID").(int)

		var swipes []models.Swipe
		db.Where("user_id = ? AND date = ?", userID, time.Now().Format("2006-01-02")).Find(&swipes)

		if len(swipes) >= 10 {
			c.JSON(http.StatusForbidden, gin.H{"error": "Daily swipe limit reached"})
			return
		}

		var profiles []models.Profile
		db.Not("id IN (?)", db.Table("swipes").Select("profile_id").Where("user_id = ? AND date = ?", userID, time.Now().Format("2006-01-02")).SubQuery()).Limit(10 - len(swipes)).Find(&profiles)

		c.JSON(http.StatusOK, profiles)
	}
}

func Swipe(db *gorm.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		userID := c.MustGet("userID").(int)
		var swipeData struct {
			ProfileID uint   `json:"profile_id"`
			Type      string `json:"type"`
		}

		if err := c.BindJSON(&swipeData); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid input"})
			return
		}

		var swipes []models.Swipe
		db.Where("user_id = ? AND date = ?", userID, time.Now().Format("2006-01-02")).Find(&swipes)

		if len(swipes) >= 10 {
			c.JSON(http.StatusForbidden, gin.H{"error": "Daily swipe limit reached"})
			return
		}

		swipe := models.Swipe{UserID: uint(userID), ProfileID: swipeData.ProfileID, Type: swipeData.Type, Date: time.Now().Format("2006-01-02")}
		db.Create(&swipe)

		c.JSON(http.StatusOK, gin.H{"message": "Swipe recorded"})
	}
}

func Purchase(db *gorm.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		userID := c.MustGet("userID").(int)
		var user models.User
		db.First(&user, userID)

		if user.Premium {
			c.JSON(http.StatusBadRequest, gin.H{"error": "User already has a premium account"})
			return
		}

		user.Premium = true
		db.Save(&user)

		c.JSON(http.StatusOK, gin.H{"message": "Premium activated"})
	}
}
