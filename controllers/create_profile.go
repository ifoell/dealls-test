package controllers

import (
	"deallsTest/models"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"net/http"
)

type CreateProfileRequest struct {
	Name string `json:"name" binding:"required"`
	Age  int    `json:"age" binding:"required"`
	Bio  string `json:"bio"`
}

func CreateProfile(db *gorm.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		var createProfileRequest CreateProfileRequest
		if err := c.BindJSON(&createProfileRequest); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid input"})
			return
		}

		userID := c.MustGet("userID").(int)
		var user models.User
		if err := db.Where("id = ?", userID).First(&user).Error; err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "User not found"})
			return
		}

		var existingProfile models.Profile
		if err := db.Where("user_id = ?", userID).First(&existingProfile).Error; err == nil {
			// Profile already exists for the user
			c.JSON(http.StatusBadRequest, gin.H{"error": "Profile already exists for the user"})
			return
		}

		profile := models.Profile{
			UserID: user.ID,
			Name:   createProfileRequest.Name,
			Age:    createProfileRequest.Age,
			Bio:    createProfileRequest.Bio,
		}

		if err := db.Create(&profile).Error; err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create profile"})
			return
		}

		c.JSON(http.StatusCreated, profile)
	}
}
