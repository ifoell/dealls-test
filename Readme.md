# Dealls Test Application

This is a simple Golang application for Dealls Test (Dating App) API.

## Installation

1. Install Go: https://golang.org/doc/install
2. Clone the repository: `git clone https://github.com/ifoell/deallsTest.git`
3. Navigate to the project directory: `cd deallsTest`
4. Install dependencies:

   ```bash
   go mod tidy
   ```
5. Import create_table_ddl.sql
6. To run the program:
   ```bash
   
   ```
7. To test the endpoint, use the postman_collection.json

## Services

1. Sign up (http://localhost:8080/signup) [type:POST]

   For new account registration
   
   Request body: 
   ```json
   {
       "username": "new_user",
       "password": "new_password"
   }
   ```
   
   Response :
   ```json
   {
      "message": "User created successfully"
   }
   ```
2. Log in (http://localhost:8080/login) [type:POST]

   For log in account after registration, and return token

   Request body: 
   ```json
   {
       "username": "username",
       "password": "password"
   }
   ```
   
   Response :
   ```json
   {
      "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTgwODQ5NDIsImp0aSI6IjIiLCJzdWIiOiJ0ZXN0dXNlcjIifQ.hs4R-sZPOh0pBWTKKopQPwG9YqCyuTPXPnd39Q9SiGk"
   }
   ```
3. Create a new profile after login (http://localhost:8080/create-profile) [type: POST]

   For create new profile
   
   Request Header:
   ```
   "Key" -> "Authorization"
   "Value" -> "token_after_login"
   ```

   Request Body:
   ```json
   {
      "name": "John Doewa",
      "age": 32,
      "bio": "Software Developer from London"
   }
   ```

   Response :
   ```json
   {
      "ID": 2,
      "CreatedAt": "2024-06-10T11:24:52.8810712+07:00",
      "UpdatedAt": "2024-06-10T11:24:52.8810712+07:00",
      "DeletedAt": null,
      "UserID": 2,
      "Name": "John Doewa",
      "Age": 32,
      "Bio": "Software Developer from London"
   }
   ```
4. Set to premium account (http://localhost:8080/purchase) [type: POST]
   
   For set account to premium account after purchasing

   Request Header:
   ```
   "Key" -> "Authorization"
   "Value" -> "token_after_login"
   ```

   Response :
   ```json
   {
      "message": "Premium activated"
   }
   ```
4. View own profile
   
   for viewing own profile

   Request Header:
   ```
   "Key" -> "Authorization"
   "Value" -> "token_after_login"
   ```
   
   Response :
   ```json
   {
        "ID": 2,
        "CreatedAt": "2024-06-10T11:24:52+07:00",
        "UpdatedAt": "2024-06-10T11:24:52+07:00",
        "DeletedAt": null,
        "UserID": 2,
        "Name": "John Doewa",
        "Age": 32,
        "Bio": "Software Developer from London"
   }
   ```
5. Swipe profile
   
   for swiping profile, left or right

   Request Header:
   ```
   "Key" -> "Authorization"
   "Value" -> "token_after_login"
   ```

   Request Body:
   ```json
   {
      "profile_id": 1,
      "type": "right"
   }
   ```

   Response :
   ```json
   {
       "message": "Swipe recorded"
   }
   ```
6. Log out
   
   for logging out and remove the token

   Request Header:
   ```
   "Key" -> "Authorization"
   "Value" -> "token_after_login"
   ```

   Response :
   ```json
   {
       "message": "Logged out successfully"
   }
   ```