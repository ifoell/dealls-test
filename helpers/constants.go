package helpers

import (
	"fmt"
	"sync"
)

var (
	JwtKey    = []byte("your_secret_key")
	Blacklist = make(map[string]bool) // In-memory blacklist
	mu        sync.Mutex
)

func AddToBlacklist(token string) error {
	mu.Lock()
	defer mu.Unlock()

	if _, exists := Blacklist[token]; exists {
		return fmt.Errorf("token already in blacklist")
	}
	Blacklist[token] = true
	return nil
}

func IsBlacklisted(token string) bool {
	mu.Lock()
	defer mu.Unlock()

	return Blacklist[token]
}
